#!/usr/bin/python

#
#  Colossus Control Software
#  By RBA Marketing
#  http://www.rbamarketing.com
#  
#  Geek Aquarium - Colossus Website
#  http://GeekAquarium.com
#  
#

import os
import ConfigParser
import time
import datetime
import sys
import glob
import mysql.connector
import wiringpi
import html
#Global Placeholders
nowtime=""

# IMPORTANT SETTINGS
#
# Set the address for the three MCP23017 chips
#

expansionaddr1=0x20
expansionaddr2=0x24
expansionaddr3=0x26

# IMPORTANT SETTINGS
#
# The channel section assigns your commands to a specific pin number.
#  

channel = {
'Air':65,
'Filter':66,
'xUnassigned27':67,
'UV':68,
'Game':69,
'heater':70,
'PartyBall':71,
'xUnassigned09':77,
'Floor':73,
'xAir2':72,
'xUnassigned04':75,
'xUnassigned05':76,
'Powerhead':74,
'xUnassigned07':80,
'Mood':79,
'xMainLight':78,
'xUnassigned10':100,
'xUnassigned11':101,
'xUnassigned12':102,
'xUnassigned13':103,
'xUnassigned14':104,
'xUnassigned15':105,
'xUnassigned16':106,
'xUnassigned17':107,
'xUnassigned18':108,
'xUnassigned19':109,
'xUnassigned20':110,
'xUnassigned21':111,
'xUnassigned22':112,
'xUnassigned22':113,
'xUnassigned24':114,
'xUnassigned25':115,
'xUnassigned26':116,
'Moonlight':200,
'Light1':201,
'Light2':202,
'xUnassigned30':203,
'xUnassigned31':204,
'xUnassigned32':205,
'xUnassigned33':206,
'xUnassigned34':207,
'xStatusPanel':208,
'xLight1':209,
'xLight2':210,
'xMoonLight':211,
'xTrimBlue':212,
'xTrimRed':213,
'xxFloorlight':214,
'xMonitorpwr2':215,
'xHDMISwitch':216,
'xStatusPanel':216}

#Manually Set Heater pin number here
heaterpin = 70
blinkpin = 79

#  Read Config Parser Variables
config = ConfigParser.ConfigParser()
scriptDirectory = os.path.dirname(os.path.realpath(__file__))
settingsFilePath = os.path.join(scriptDirectory, "config/colossus.config")
#print settingsFilePath
config.read(settingsFilePath)

#Assign config variables
debug = config.get('BlackDogAquarium','debug')
version = config.get('BlackDogAquarium','version')

expansionpinbase1 = config.get('mcp23017','expansionpinbase1')
expansionpinbase2 = config.get('mcp23017','expansionpinbase2')
expansionpinbase3 = config.get('mcp23017','expansionpinbase3')

numberprobes = int(config.get('ds18b20','numberprobes'))
systemtemp = config.get('ds18b20','systemtemp')
systemtempname = config.get('ds18b20','systemtempname')
tempid_1 = config.get('ds18b20','tempid_1')
tempname_1= config.get('ds18b20','tempname_1')
tempid_2 = config.get('ds18b20','tempid_2')
tempname_2= config.get('ds18b20','tempname_2')

dht11id = config.get('dht11','dht11id')
dht11name = config.get('dht11','dht11name')

mysqllog = config.get('mysql','mysqllog') 
mysqlserver = config.get('mysql','mysqlserver')
mysqldatabase = config.get('mysql','mysqldatabase')
mysqlusername = config.get('mysql','mysqlusername')
mysqlpassword = config.get('mysql','mysqlpassword')


#initalize the mcp23017 chips
wiringpi.wiringPiSetup()
wiringpi.mcp23017Setup(int(expansionpinbase1),expansionaddr1)
wiringpi.mcp23017Setup(int(expansionpinbase2),expansionaddr2)
wiringpi.mcp23017Setup(int(expansionpinbase3),expansionaddr3)

#initialize all channels to output
for x,y in channel.items():
    wiringpi.pinMode(int(y),1)
        
# This class provides the functionality we want. You only need to look at
# this if you want to know how this works. It only needs to be defined
# once, no need to muck around with its internals.
# SRC: http://code.activestate.com/recipes/410692/

class switch(object):
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration
    
    def match(self, *args):
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True
        elif self.value in args: # changed for v1.5, see below
            self.fall = True
            return True
        else:
            return False

def nowtime():
    #This function gets the current date/time
    nowtime=datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S")
    return nowtime

def writephlog(phlevel):
    #This function records the PH to the mysql datadase.
    if mysqllog == "TRUE":
        cnx = mysql.connector.connect(user=mysqlusername,               password=mysqlpassword,  
                                     host=mysqlserver,
                                      database=mysqldatabase)
        cursor = cnx.cursor()
        mynowtime=nowtime()
        add_event = ("insert into waterph"
                    "(phid, date, phlevel) "
                    " values (%s, %s, %s)")
        data_event = ('',mynowtime,phlevel)
        cursor.execute(add_event,data_event)
        cnx.commit()
        cursor.close()
        cnx.close()
        print "[" + mynowtime +"] PH Level: "+str(phlevel)
        return;
    else:
        print "[" + mynowtime +"] PH Level: "+str(phlevel)
            
    return

def writeeventlog(sensor, eventdata):

    #This function writes EVENT data to stdin and mysql database if turned on.
    mynowtime=nowtime()
    if mysqllog == "TRUE":
        cnx = mysql.connector.connect(user=mysqlusername,               password=mysqlpassword,  
                                     host=mysqlserver,
                                      database=mysqldatabase)
        cursor = cnx.cursor()
        mynowtime=nowtime()
        add_event = ("insert into events"
                    "(eventid, date,sensor,eventdata) "
                    " values (%s, %s, %s, %s)")
        data_event = ('',mynowtime,sensor,eventdata)
        cursor.execute(add_event,data_event)
        cnx.commit()
        cursor.close()
        cnx.close()
        print "[" + mynowtime +"] " + sensor + ": "+str(eventdata)
        return;
    else:
        print "[" + mynowtime +"] " + sensor + ": "+str(eventdata)
            
    return
    
def writetemplog(sensor,temperature,humidity):
    #This function writes temperature data to stdin and mysql database if turned on.
    if debug == "TRUE":
        print "(writetemplog) Called "
        print "(writetemplog) Mysllog: "+mysqllog
    mynowtime=nowtime()
    if mysqllog == "TRUE":
        cnx = mysql.connector.connect(user=mysqlusername, password=mysqlpassword,  
                                      host=mysqlserver,
                                      database=mysqldatabase)
        cursor = cnx.cursor()
        mynowtime=nowtime()
        add_temp = ("insert into temperatures"
                    "(id, date,sensor,temperature,humidity) "
                    " values (%s, %s, %s, %s, %s)")
        data_temp = ('',mynowtime,sensor,float(temperature),float(humidity))
        cursor.execute(add_temp,data_temp)
        cnx.commit()
        cursor.close()
        cnx.close()
        print "[" + mynowtime +"] " + sensor + ": Temperature:  "+str(temperature)+" humidity: "+str(humidity)
        return;
    else:
        print "[" + mynowtime +"] " + sensor + ": Temperature:  "+str(temperature)+" humidity: "+str(humidity)
        
    #if sensor == "Colossus-Tank-Left":
    #   if int(temperature) >= 79:
           #Turn off heaters
           #flipchannel(pin, sensor, channelcmd)
    #       flipchannel(67,"Heater","off")
    #       flipchannel(70,"Heater2","off")
    #   if int(temperature) <= 77:
           #turn on heaters
    #       flipchannel(67,"Heater","on")
    #       flipchannel(70,"Heater2","on")
    return
    
def read_temp(device_file):
    #This function is needed by the read_ds_probes()
    if debug == "TRUE":
        print "device file " + device_file
        print "read temp"
    lines = read_temp_raw(device_file)
    if debug == "TRUE":
        print lines[0]
        print lines[0].strip()[-3:]
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw(device_file)
    if debug == "TRUE":
        print "Here are the lines"
        print lines
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return  temp_f
		
def read_temp_raw(device_file):
    #This function is needed by the read_ds_probes()
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_ds_probes():
    #Read all DS18B20 Probes
    if debug == "TRUE":
       print "(read_ds_probes) called"
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')
    base_dir = '/sys/bus/w1/devices/'
    if debug == "TRUE":
        print "(read_ds_probes) "+base_dir
    for x in range( 0, numberprobes):
        if x == 0:
            device_folder = base_dir + systemtemp
            device_file = device_folder + '/w1_slave'
            if debug == "TRUE":
                print "(read_ds_probes) "+device_file
            temperature = read_temp(device_file)
            #print temperature
            if debug == "TRUE":
                print "(readdsprobes) Calling writetemplog on "+systemtempname
            writetemplog(systemtempname,temperature,0)
        if x == 1:
            device_folder = base_dir + tempid_1
            device_file = device_folder + '/w1_slave'
            if debug == "TRUE":
                 print "(read_ds_probes) "+device_file
                 print "(read_ds_probes) Calling read_temp"
            temperature = read_temp(device_file)
            #print temperature
            if debug == "TRUE":
                print "(read_ds_probes) Calling writetemplog on "+tempname_1
    	    writetemplog(tempname_1,temperature,0)
    	    
        if x == 2:
            device_folder = base_dir + tempid_2
            device_file = device_folder + '/w1_slave'
            if debug == "TRUE":
                 print "(read_ds_probes) "+device_file
                 print "(read_ds_probes) Calling read_temp"
            temperature = read_temp(device_file)
            #print temperature
            if debug == "TRUE":
                print "(read_ds_probes) Calling writetemplog on "+tempname_2
    	    writetemplog(tempname_2,temperature,0)
    	    #Heatertemp variable used to turn heater on and off
    	    heatertemp = temperature
        
    #check heater current status
    heaterstatus = wiringpi.digitalRead(int(heaterpin))
    if debug == "TRUE":
        print "Heaterstatus: "
        print heaterstatus
    
    #check heatertemp and turn on/off heater 
    if heatertemp <= 77:
        if heaterstatus == 0:
            if debug == "TRUE":
                print "(read_ds_probes) Calling flipchannel(70,heater2,on)"
            flipchannel(heaterpin,"AutoHeat","on")
           # writeeventlog("Heater2","ON")
            
    if heatertemp >= 79:
        if heaterstatus == 1:
            if debug == "TRUE":
                print "(read_ds_probes) Calling flipchannel(70,heater2,off)"
            flipchannel(heaterpin,"AutoHeat","off")
            
    if heatertemp >= 82:
        if heaterstatus == 1:
            if debug == "TRUE":
                print "(read_ds_probes) Calling flipchannel(70,heater2,off)"
            flipchannel(heaterpin,"Autoheat","off")
        blink(blinkpin,"Mood")
        
    return

    
def flipchannel(pin, sensor, channelcmd):
    #The functions flips the mcp23017 to off/on
    
    #define needed variables
    cmd=""
    action = 0
    
    
    #Check to see if a specific on/off command was sent 
    #if so take the appropiate action.  
    if debug == "TRUE":
        print "(Flipchannel) channelcmd" +str(channelcmd)
    if channelcmd == "":
        pinstatus = wiringpi.digitalRead(int(pin))
        if debug == "TRUE":
            print "(Flipchannel) Current Pin Status: "+str(pinstatus)
        if pinstatus == 0:
            action = 1
            cmd = "On"
        if pinstatus == 1:
            action = 0
            cmd = "Off"
    elif  channelcmd.lower() == "off":
        action = 0
        cmd = "Off"
    elif channelcmd.lower() == "on":
        action = 1
        cmd = "On"
    if debug == "TRUE":
        print "(Flipchannel) Cmd: "+cmd
        print "(Flipchannel) Pin: "+str(pin)
        print "(Flipchannel) Action: "+str(action)
        print "(Flipchannel) Calling digitalWrite"
    wiringpi.digitalWrite(int(pin),int(action))
    pinstatus = wiringpi.digitalRead(int(pin))
    
    if debug == "TRUE":
        print "(Flipchannel) New Pin Status: "+str(pinstatus)
        print "(Flipchannel) Calling writeeventlog"
    writeeventlog(sensor,cmd.upper())
    return


def printallcommands():
    if debug == "TRUE":
        print "(PrintAllCommands) Printing All Commands"
    print "Colossus Control Center Version "+str(version)
    print "==================================="
    print "System Commands"
    print "==================================="
    print "Halt"
    print "Temp"
    print "Status [html]"
    print "PHTest x.xx"
    print "log sensor data"
    print "waterchange xx"
    print ""
    print "==================================="
    print "Channel Commands"
    print "==================================="
    
    for key,value  in sorted(channel.items()):
        print key, value
    return

def halt():
    # This function turns off all mcp23017 FAST!
    if debug == "TRUE":
        print "(Halt) Halting System"
    for x,y in channel.items():
        wiringpi.digitalWrite(int(y),0)
    
    #turn off the lights (Light relay is active low)
    wiringpi.digitalWrite(200,1)
    wiringpi.digitalWrite(201,1)
    wiringpi.digitalWrite(202,1)

        
    writeeventlog("Colossus","System Halted")
    if debug == "TRUE":
        print "(Halt) System Halted"
        
        
def allon():
    # This function turns off all mcp23017 FAST!
   # if debug == "TRUE":
   #     print "(AllOn) Turning All Channels On"
   # for pinx in range(65,81):
   #     wiringpi.pinMode(pinx,1)
   # for pinx in range(100,116):
   #     wiringpi.pinMode(pinx,1)
   # for pinx in range(200,216):
   #     wiringpi.pinMode(pinx,1)
   #     
   # writeeventlog("Colossus","All Channels On")
   # if debug == "TRUE":
   #     print "(AllOn) All Channels On"
   return     
 
def saytemp():
    cnx = mysql.connector.connect(user=mysqlusername, password=mysqlpassword,  
                                      host=mysqlserver,
                                      database=mysqldatabase)
    cursor = cnx.cursor()
    mynowtime=nowtime()
    query = ("""select temperature from temperatures where id > 1 order by id desc limit 1""")
    cursor.execute(query)
    data = cursor.fetchall()
    print data
    output = []
    for temperature in data:
        output.append(float(temperature))
    cursor.close()
    cnx.close()

def webstatus():
    outputstring=""
    for x,y in channel.items():
        #wiringpi.pinMode(int(y),1)
        status = wiringpi.digitalRead(int(y))
        if "ight" in x:
            if status == 1:
                status = 0
            else:
                status = 1
        if "x" not in x: 
            outputstring +=  str(x)+","+str(status)+","
    # This line removes the trailing , on the output string
    outputstring = outputstring[:-1]
    print outputstring
    
    
    
def hdmipower(switchcmd):
    if switchcmd.lower() =="on":
        syscmd = "vcgencmd display_power 1"
        os.system(syscmd)
        writeeventlog("HDMI", "ON")
    elif switchcmd.lower() ==  "off":
        syscmd = "vcgencmd display_power 0"
        os.system(syscmd)
        writeeventlog("HDMI", "OFF")
    
    
    
    
    
    
    
def status(switchcmd):
    #
    # This function prints out the status for all commands not containing the Letter X
    if switchcmd.lower() == "html":
        screenmode = "html"
        html.output_html_header_refresh()
        #print table for html
        print "<table>"
    else:
        screenmode = "console"
    #print screenmode
    
    for x,y in channel.items():
        #wiringpi.pinMode(int(y),1)
        status = wiringpi.digitalRead(int(y))
        if "ight" in x:
            if status == 1:
                status = 0
            else:
                status = 1
        if "x" not in x: 
            if screenmode == "console":
                print x, y, status
            else:
                if status == 0:
                    colorclass = "w3-text-gray"
                else:
                    colorclass = "w3-text-blue  fa-spin "
                print '<tr><td class="">'
                print '<i class="fa  fa-cog fa-lg '+colorclass+'"></i> '
		print '<span class="'+colorclass+'">'
                print x, status
                print "<span></td></tr>"
    if screenmode == "html":
        #close table
        print "</table>"
        html.output_html_footer()

def flipchannelnolog(pin, sensor, channelcmd):
    #
    # THIS FUNCTION DOES PERFORM MYSQL LOGGING
    #
    #The functions flips the mcp23017 to off/on
    #
    
    #define needed variables
    cmd=""
    action = 0
    
    
    #Check to see if a specific on/off command was sent 
    #if so take the appropiate action.  
    if debug == "TRUE":
        print "(Flipchannel) channelcmd" +str(channelcmd)
    if channelcmd == "":
        pinstatus = wiringpi.digitalRead(int(pin))
        if debug == "TRUE":
            print "(Flipchannel) Current Pin Status: "+str(pinstatus)
        if pinstatus == 0:
            action = 1
            cmd = "On"
        if pinstatus == 1:
            action = 0
            cmd = "Off"
    elif  channelcmd.lower() == "off":
        action = 0
        cmd = "Off"
    elif channelcmd.lower() == "on":
        action = 1
        cmd = "On"
    if debug == "TRUE":
        print "(Flipchannel) Cmd: "+cmd
        print "(Flipchannel) Pin: "+str(pin)
        print "(Flipchannel) Action: "+str(action)
        print "(Flipchannel) Calling digitalWrite"
    wiringpi.digitalWrite(int(pin),int(action))
    pinstatus = wiringpi.digitalRead(int(pin))
    
    if debug == "TRUE":
        print "(Flipchannel) New Pin Status: "+str(pinstatus)
        print "(Flipchannel) Calling writeeventlog"
    return

def blink(channeltoblink, sensor):
    cmd = "Blink Issued"
    writeeventlog(sensor,cmd.upper())
    for a in range (100):
        flipchannelnolog(channeltoblink, '','')
        time.sleep(.1)
    
def waterchange(gallons):
    
    if mysqllog == "TRUE":
        nowtime=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        cnx = mysql.connector.connect(user=mysqlusername, password=mysqlpassword,
        host=mysqlserver,
        database=mysqldatabase)
        cursor = cnx.cursor()
        add_temp = ("insert into waterchanges"
            "(wcid, date,gallons) "
            " values (%s, %s, %s)")
        data_temp = ('',nowtime,gallons)
        cursor.execute(add_temp,data_temp)
        cnx.commit()
        cursor.close()
        cnx.close()
        nowtime=datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S")
        print "[" + nowtime +"] Water Change "+ str(gallons)+" Gallons"
    else:
        print "error"


def main(argv):
    # My code here
    
    if len(sys.argv) > 2:
        switchcmd = sys.argv[2]
    else:
        switchcmd = ""
    if debug == "TRUE":
        print "Switch cmd = " + switchcmd
            
    
    
    if debug == "TRUE":
        print "(main) main executed"
        print "(main) Version: "+str(version)
        print "(main) Argv[1]: "+argv[1]
        
    # The following example is pretty much the exact use-case of a dictionary,
    # but is included for its simplicity. Note that you can include statements
    # in each suite.
    # SRC: http://code.activestate.com/recipes/410692/
   
    for case in switch(argv[1].lower()):
        if case('log'):
            sensor=sys.argv[2]
            eventdata = sys.argv[3]
            if debug == "TRUE":
                print "(MainCase) Calling writeeventlog(" + sensor +"," + eventdata +")"
            writeeventlog(sensor, eventdata)
            break
        if case('temp'):
            if debug == "TRUE":
                print "(Main Case) Temp was Selected"
                print "(Main Case) Calling read_ds_probes"
            read_ds_probes()
            
            ### ADD DHT11 Function call here
            break
        if case('webstatus'):
            webstatus()
            break
        
        if case('help'):
            if debug == "TRUE":
                print "(Main Case) Help was Selected"
            printallcommands()
            break
        if case('halt'):
            if debug == "TRUE":
                print "(Main Case) Halt was Selected"
                print "(Main Case) Calling Halt"
            halt()
            break
        
        if case('hdmi'):
            hdmipower(switchcmd)
            break;
             
        if case('allon'):
            allon()
            break
        
        if case('status'):
            status(switchcmd)
            break
        
        if case('saytemp'):
            saytemp()
            break
        
        if case('blink'):
            blink(blinkpin,"Mood")
            break

        if case('phtest'):
            phlevel = sys.argv[2]
            writephlog(phlevel)
            break
            
        if case('waterchange'):
            waterchange(sys.argv[2])
            break
        
        #if case(channelname_1.lower()):
            #if debug == "TRUE":
                #print "(Main Case) Channel Name: "+channelname_1
                ##print "(Main Case) Channel Command: "+""
                #print "(Main Case) Calling flipchannel()"
            #flipchannel(channelid_1, channelname_1, "")
            #break
        
        if case(): # default, could also just omit condition or 'if True'
            if debug == "TRUE":
                print "(Main Case Channel Loop) entered"
                print "(Main Case Channel Loop) Searching for argv[1]"
            # No need to break here, it'll stop anyway
            for k, v in channel.items():
                for case in switch(argv[1].lower()):
                    if case(k.lower()):
                        #print k, v
                        if debug == "TRUE":
                            print "(Main Case Channel Loop) Found Command: " + k
                            print "(Main Case) Calling flipchannel("+str(v)+","+str(k)+","+switchcmd+")"
                        
                        flipchannel(v, k, switchcmd)
                        break
                   



    
    
        
if __name__ == "__main__":
    main(sys.argv)
