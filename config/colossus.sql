-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 19, 2017 at 07:50 PM
-- Server version: 5.5.57-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `colossus`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
`eventid` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `sensor` varchar(25) NOT NULL,
  `eventdata` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2382 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `security`
--

CREATE TABLE IF NOT EXISTS `security` (
`eventid` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `sensor` varchar(25) NOT NULL,
  `eventdata` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temperatures`
--

CREATE TABLE IF NOT EXISTS `temperatures` (
`id` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `sensor` varchar(25) NOT NULL,
  `temperature` decimal(11,2) NOT NULL,
  `humidity` decimal(11,2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=306734 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `waterchanges`
--

CREATE TABLE IF NOT EXISTS `waterchanges` (
`wcid` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `gallons` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `waterph`
--

CREATE TABLE IF NOT EXISTS `waterph` (
`phid` bigint(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `phlevel` decimal(4,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='waterph';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
 ADD PRIMARY KEY (`eventid`), ADD KEY `date` (`date`), ADD KEY `sensor` (`sensor`);

--
-- Indexes for table `security`
--
ALTER TABLE `security`
 ADD PRIMARY KEY (`eventid`), ADD KEY `date` (`date`), ADD KEY `sensor` (`sensor`);

--
-- Indexes for table `temperatures`
--
ALTER TABLE `temperatures`
 ADD PRIMARY KEY (`id`), ADD KEY `date` (`date`), ADD KEY `sensor` (`sensor`);

--
-- Indexes for table `waterchanges`
--
ALTER TABLE `waterchanges`
 ADD PRIMARY KEY (`wcid`), ADD KEY `date` (`date`);

--
-- Indexes for table `waterph`
--
ALTER TABLE `waterph`
 ADD PRIMARY KEY (`phid`), ADD KEY `date` (`date`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
MODIFY `eventid` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2382;
--
-- AUTO_INCREMENT for table `security`
--
ALTER TABLE `security`
MODIFY `eventid` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temperatures`
--
ALTER TABLE `temperatures`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=306734;
--
-- AUTO_INCREMENT for table `waterchanges`
--
ALTER TABLE `waterchanges`
MODIFY `wcid` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `waterph`
--
ALTER TABLE `waterph`
MODIFY `phid` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
